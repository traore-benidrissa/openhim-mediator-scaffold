'use strict'

import express from 'express'
import { registerMediator, activateHeartbeat } from 'openhim-mediator-utils'
import mediatorConfig, { urn } from './mediatorConfig.json'
import { fetchConfig } from 'openhim-mediator-utils'
const fetch = require('node-fetch');
const request = require("request");
global.fetch = fetch
global.Headers = fetch.Headers;

const app = express()
/*
 *utiliser c'est configurations pour exécuter en server node hors docker
 *
 */
const openhimConfig = {
    username: 'root@openhim.org',
    password: 'P@ssw0rd',
    apiURL: 'https://localhost:8080',
    trustSelfSigned: true,
    urn
}
/*
 *utiliser cette configurations pour exécuter en docker
 *
const openhimConfig = {
    username: 'root@openhim.org',
    password: 'P@ssw0rd',
    apiURL: 'https://openhim-core:8080',
    trustSelfSigned: true,
    urn
}
*/

registerMediator(openhimConfig, mediatorConfig, err => {
    if (err) {
        console.error('Failed to register mediator. Check your Config:', err)
        process.exit(1)
    }
})

fetchConfig(openhimConfig, (err, initialConfig) => {
    console.log(openhimConfig)
    if (err) {
        console.error(err)
        process.exit(1)
    }
    console.log('Initial Config: ', JSON.stringify(initialConfig))
})


const emitter = activateHeartbeat(openhimConfig)
emitter.on('error', err => {
    console.error('Heartbeat failed: ', err)
})

emitter.on('config', newConfig => {
    console.log('Received updated config:', JSON.stringify(newConfig))
})

app.all('/api/*', (req, res) => {
    let username ="";
    let password ="";

    let postApiUrl= "https://postman-echo.com/post";
    let data = {
        "program": "eBAyeGv0exc",
        "orgUnit": "DiszpKrYNg8",
        "eventDate": "2013-05-17",
        "status": "COMPLETED",
        "storedBy": "admin",
        "dataValues": [
          { "dataElement": "qrur9Dvnyt5", "value": "" },
          { "dataElement": "oZg33kd9taw", "value": "" },
          { "dataElement": "msodh3rEMJa", "value": "" }
        ]
      }

    let options = {
        method: 'POST',
        url: postApiUrl,
        body: JSON.stringify(data),	
        headers: { 
            'Authorization':  "Basic " + Buffer.from(username + ':' + password, 'base64').toString('ascii'),
            'Accept': 'application/json',
            'Content-Type': 'application/json' 
        },
        from: {
        mimeType: 'application/json'
        }
    }; 

    // get json response		
    request(options, function(error, response, body) {
        let getResponse = JSON.stringify(JSON.parse(body));
        if(error){
            let openhimResultFormat =
                {
                    "x-mediator-urn": urn, //same as the mediator's urn
                    "status": "Successful",
                    "response": {
                        "status": 1,
                        "headers": req.header,
                        "body": "Post to DHIS2 failed:" + JSON.stringify(error),
                        "timestamp": new Date()
                    }
                    ,
                    "error": { 
                        "message": "Post to DHIS2 failed:",
                        "stack": JSON.stringify(error)
                    }
                };
            // set content type header so that OpenHIM knows how to handle the response
            res.set('Content-Type', 'application/json+openhim');
            res.send(openhimResultFormat);
        }
        
        let openhimResultFormat =
        {
            "x-mediator-urn": urn, //same as the mediator's urn
            "status": "Successful",
            "response": {
                "status": 1,
                "headers": req.header,
                "body": JSON.stringify(getResponse),
                "timestamp": new Date()
            }
        };
        res.send(openhimResultFormat);
    });
});


app.all('*', (req, res) => {

    let openhimResultFormat =
    {
        "x-mediator-urn": urn, //same as the mediator's urn
        "status": "Successful",
        "response": {
            "status": 1,
            "headers": req.header,
            "body": "default endpoint!",
            "timestamp": new Date()
        }
    };
    res.send(openhimResultFormat);
});

app.listen(3000, () => {
    console.log('Server listening on port 3000...')
    activateHeartbeat(openhimConfig)
})



